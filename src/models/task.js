//como luciran las tareas
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
//define como luciranlos datos con el schema
const TaskSchema = new Schema({
    title: String,
    description: String,
    status: {
        type: Boolean,
        default: false
    }
});

//utilizar el schema pasandolo al metodo model
module.exports = mongoose.model('task', TaskSchema)
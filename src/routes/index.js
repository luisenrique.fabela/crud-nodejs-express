const express = require('express');
const router = express.Router();
//
//mostrar en pantalla
//cuandose pida una peticion get a la ruta inicial de mi servidor
//respondere con un mensaje que diaga hello world
/*
    router.get('/', (req, res) => {
        res.send('Hello world')
    });
*/
const Task = require('../models/task');
//consultar datos 
router.get('/', async(req, res) => {
    const tasks = await Task.find(); //traer los datos de la base de datos
    console.log(tasks)
    res.render('index', {
        tasks //tasks:tasks

    });
    //res.render('index', { title: 'hola' });
});

router.post('/add', async(req, res) => {
    const task = new Task(req.body); //tengo un nuevo dato como objeto
    //console.log(req.body);
    await task.save(req.body); //.then //voy a almacenarlo
    //res.send('recibido'); //cuando se almacene, envio al navegador recibido
    res.redirect('/');
});

//boton done
router.get('/done/:id', async(req, res) => {
    //cambio el status
    const { id } = req.params; //recibo el id
    const task = await Task.findById(id); //lo busco y encuentro la tarea y lo almaceno en la constante
    task.status = !task.status; //una vez tengo la tarea cambio el estado de false a true y viceversa
    await task.save(); //una vez cambiado lo guardo en la base de datos
    res.redirect('/');
})

//edit
router.get('/edit/:id', async(req, res) => {
    const { id } = req.params;
    const task = await Task.findById(id);
    res.redirect('edit', { //redirecciono a edit.ejs
        task
    });
})

//delete
router.get('/delete/:id', async(req, res) => {
    const { id } = req.params;
    await Task.remove({ _id: id });
    res.redirect('/');
})

module.exports = router;
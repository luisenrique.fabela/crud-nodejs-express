const path = require('path');
const morgan = require('morgan')
const mongoose = require('mongoose');
//utilizar express y empezar a inicializarlo
const express = require('express');

const app = express(); // aplicacion del servidor

//conectando a la base de datos
mongoose.connect('mongodb://localhost/crud-mongo')
    .then(db => console.log('BD Conectada'))
    .catch(err => console.log(err));


//importing routes
const indexRoutes = require('./routes/index')

//settings

//definir puerto
app.set('port', process.env.PORT || 3000)
    //le digo la ubicacion de las vistas

app.set('views', path.join(__dirname, 'views')); //utilizo path para concatenar un\ 
//motor de plantilla ejs
app.set('view engine', 'ejs');



// middlewares: funcion que se ejecuta antes de llegar a la ruta
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false })) //para entender los datos que me envia un formulario


//rutas
app.use('/', indexRoutes);

//starting the server 
//Escuchar el servidor en el puerto 3000
app.listen(app.get('port'), () => {
    console.log(`Server en puerto ${app.get('port')}`);
})